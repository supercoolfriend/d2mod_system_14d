#include <Windows.h>

#define MOD_SCRIPTS_PATH "scripts"
#define MOD_GS_PATH "scripts\\gs114d.dll"

typedef struct ModModule
{
    char* author;
    char* version;
    char* description;
    void* ops;
}ModModule;

#ifdef __cplusplus
extern "C" {
#endif

    __declspec(dllexport) void __stdcall Init();
    __declspec(dllexport) void __stdcall Release();
    /*this method will be exposed to other modules*/
    __declspec(dllexport) ModModule* __stdcall GetInfo();

#ifdef __cplusplus
}
#endif